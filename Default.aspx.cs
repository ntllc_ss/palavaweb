﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Globalization;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Data objData = new Data();
        //string[] str = { "123", "456", "789" };
        //objData.data = str;
        //string[] strtemp = objData.data;
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        try
        {
            //string[] str = { "123", "456", "789" };

            //Data.data = str;

            string filename = FileUpload1.FileName;

            string fullpath = @"~/" + filename + "";
            FileUpload1.SaveAs(Server.MapPath(fullpath));

            string path = Server.MapPath(@"~/" + filename + "");
            string[] lines = File.ReadAllLines(path);

            Data.data = lines;

            System.IO.File.Delete(Server.MapPath(fullpath));
        }
        catch (Exception)
        {
            Response.Write("Something wrong, Upalod again..");
        }
    }


    protected void btnDownload_Click(object sender, EventArgs e)
    {
        try
        {
            string fileName = @"" + Server.MapPath("Sample.txt");

            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", "attachment;filename=Sample.txt");

            Response.WriteFile(fileName);

            Response.Flush();
            Response.Close();
        }
        catch (Exception) { }
    }
}